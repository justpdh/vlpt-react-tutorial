//import logo from './logo.svg';
import './App.scss';

import E0101 from './epic01/e01/E0101';
import E0102 from './epic01/e02/E0102';
import E0103 from './epic01/e03/E0103';
import E0104 from './epic01/e04/E0104';
import E0105 from './epic01/e05/E0105';
import E0106 from './epic01/e06/E0106';
import E0107 from './epic01/e07/E0107';
import E0108 from './epic01/e08/E0108';
import E0109 from './epic01/e09/E0109';
import E0110 from './epic01/e10/E0110';
import E0111 from './epic01/e11/E0111';
import E0112 from './epic01/e12/E0112';
import E0113 from './epic01/e13/E0113';

import E0201 from './epic02/e01/E0201';
import E0202 from './epic02/e02/E0202';
import E0203 from './epic02/e03/E0203';
import E0204 from './epic02/e04/E0204';

function App() {
    // return (<E0101 />);
    // return (<E0102 />);
    // return (<E0103 />);
    // return (<E0104 />);
    // return (<E0105 />);
    // return (<E0106 />);
    // return (<E0107 />);
    // return (<E0108 />);
    // return (<E0109 />);
    // return (<E0110 />);
    // return (<E0111 />);
    // return (<E0112 />);
    // return (<E0113 />);

    // return (<E0201 />);
    // return (<E0202 />);
    // return (<E0203 />);
    return (<E0204 />);
}

export default App;
