import React from 'react';

const Hello = ({name, color, isSpecial}) => {
    return (
        <div style={{color:color}}>
            {isSpecial && <b>*</b>}
            Hello {name}
        </div>
    )
}

Hello.defaultProps = {
    name:'DefaultName'
};

export default Hello;