import React, { useEffect, useContext } from 'react';
import { UserDispatch } from './E0109';

function User({ user }) {

    console.log('call--User');

    const dispatch = useContext(UserDispatch);

    useEffect(() => {
        console.log('user 값이 설정됨', user);

        return () => {
            console.log('user 가 바뀌기 전..', user);
        };
    }, [user]);

    const style = {
        cursor: 'pointer',
        color: user.active ? 'green' : 'black'
    };

    return (
        <div>
            <b>{user.id}</b>
            &nbsp;&nbsp;&nbsp;
            <b
                style={style}
                onClick={() => {
                    dispatch({ type: 'TOGGLE_USER', id: user.id });
                }}
            >
                {user.username}
            </b>
            &nbsp;&nbsp;&nbsp;
            <span>{user.email}</span>
            &nbsp;&nbsp;&nbsp;
            <button onClick={() => { dispatch({ type: 'REMOVE_USER', id: user.id }); }} >Remove</button>
        </div>
    );
}

function UserList({ users, onRemove, onToggle }) {
    return (
        <div>
            {users.map((user, index) => (
                <User
                    key={index}
                    user={user}
                    onRemove={onRemove}
                    onToggle={onToggle}
                />
            ))}
        </div>
    );
}

export default UserList;
