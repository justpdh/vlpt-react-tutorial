import React from 'react';
import produce from 'immer';


function E0110() {

    const state = {
        posts: [
            {
                id: 1,
                title: '제목입니다.',
                body: '내용입니다.',
                comments: [
                    {
                        id: 1,
                        text: '와 정말 잘 읽었습니다.'
                    }
                ]
            },
            {
                id: 2,
                title: '제목입니다.',
                body: '내용입니다.',
                comments: [
                    {
                        id: 2,
                        text: '또 다른 댓글 어쩌고 저쩌고'
                    }
                ]
            }
        ],
        selectedId: 1
    };
    console.log(state);

    const nextState = {
        ...state,
        posts: state.posts.map(post =>
            post.id === 1
                ? {
                    ...post,
                    comments: post.comments.concat({
                        id: 3,
                        text: '새로운 댓글'
                    })
                }
                : post
        )
    };
    console.log(nextState);

    const nextStateByImmer = produce(state, draft => {
        const post = draft.posts.find(post => post.id === 1);
        post.comments.push({
            id: 3,
            text: '와 정말 쉽다!'
        });
    });
    console.log(nextStateByImmer);


    return (
        <div>
            <b>Immter test</b>
        </div>
    )
}



export default E0110;