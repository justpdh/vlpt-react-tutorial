import Hello from './Hello';
import Wrapper from './Wrapper';

function E0111() {
    return (
        <div>
            <Wrapper>
                <Hello name="react" color="green" isSpecial={true} />
                <Hello color="red" />
            </Wrapper>
        </div>
    );
}

export default E0111;