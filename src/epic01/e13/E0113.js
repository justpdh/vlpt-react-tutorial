import User from "./User";
import ErrorBoundary from "./ErrorBoundary";

function E0113() {

    const user = {
        id: 1,
        username: "velopert",
    };

    return (
        <div>
            <ErrorBoundary>
                <User />
            </ErrorBoundary>
        </div>
    );
}

export default E0113;
